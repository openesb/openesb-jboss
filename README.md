openesb-jboss
=============

Jboss 5 runtime for openesb

Required software:
  * Maven 2.2.1
  * Java 1.5+
  * JBoss 5.1.0 GA
  * OpenESB on JBoss 1.1
  * OpenJBI Components 2.3.0

The master of the OpenESB on JBoss must first be built using maven.
You will find the config.xml we are using to compile on the jenkins (which adds our maven repository http://nexus.openesb-dev.org:8081/nexus/)

  Building the OpenESB on JBoss project will produce the following artifact:
  packaging\jboss-dist\bld\openesb-jboss5-dist-1.1-distribution.zip

After exploding that zip file, perform the following steps:
  1) copy the jboss5-jbi-install folder to the root of the JBoss installation (e.g.: jboss-5.1.0.GA\)

  2) copy the openesb-jboss5-deployer.jar file to the deployers directory of the JBoss server instance (e.g.: jboss-5.1.0.GA\server\default\deployers)
  
  3) copy the openesb-jboss5.sar file to the deploy directory of the JBoss instance (e.g.: jboss-5.1.0.GA\server\default\deploy)
  
  4) Create the following directories under the JBoss server instance (e.g.: jboss-5.1.0.GA\server\default\):
    jbi/autoinstall
    jbi/autodeploy
  
  5) Start up JBoss.  Once JBoss has started, move the JBI shared lib components (e.g.: encoderlib.jar, wsdlextlib.jar, wsdlsl.jar) the to jbi/autoinstall directory.  Example JBoss log output after installing the shared libs:
    07:51:38,729 INFO  [management] Installing shared library sun-encoder-library to target default.
    07:51:38,789 INFO  [framework] JBIFW1300: Shared Library sun-encoder-library has been installed.
    07:51:39,279 INFO  [management] Installing shared library sun-wsdl-ext-library to target default.
    07:51:39,349 INFO  [framework] JBIFW1300: Shared Library sun-wsdl-ext-library has been installed.
    07:51:39,579 INFO  [management] Installing shared library sun-wsdl-library to target default.
    07:51:39,639 INFO  [framework] JBIFW1300: Shared Library sun-wsdl-library has been installed.
  
  6) Move the rest of the BC and SE jars to jbi/autoinstall, one by one; if there are any interdependencies between the JBI components, the components without dependencies should be moved to the jbi/autoinstall directory first.  Once all of the JBI components are installed, JBI SAs can be installed by moving them into the jbi/autodeploy directory.

