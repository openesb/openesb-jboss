/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBoss5JBIBootstrap.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.jboss5;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;


/**
 * This is the bootstrap class for the JBI runtime framework. It implements the
 * JBoss App Server <CODE>Service</CODE> interface and is responsible
 * for creating the class loader hierarchy required by the JBI runtime.
 *
 * @author Sun Microsystems Inc.
 */
public class JBoss5JBIBootstrap implements JBoss5JBIBootstrapMBean {
    /**
     * JBI Framework for JBoss
     */
    private static final String JBI_FRAMEWORK_CLASS_NAME = "com.sun.jbi.framework.jboss5.JBoss5ASJBIFramework";

    /**
     * System property for the JBoss Server Name
     */
    private static final String JBOSS_SERVER_NAME = "jboss.server.name";

    /**
     * System property for the JBoss Server Home URL
     */
    private static final String JBOSS_HOME_URL = "jboss.server.home.url";

    /** JSR208 interfaces. */
    private static final String JBI_JAR_NAME = "jbi.jar";

    /** JBI runtime interfaces exposed to components. */
    private static final String JBI_EXT_JAR_NAME = "jbi-ext.jar";

    /**
     * The Framework ClassLoader
     */
    public static ClassLoader mFrameworkClassLoader;

    /**
     * Instance of the JBI Framework.
     */
    private Object mJbiFramework;

    /**
     * Environment Properties
     */
    private Properties mEnvironment;

    /**
     * List of jars that should not be included in the runtime classloader.
     */
    private List<String> mBlacklistJars = new ArrayList<String>();

    /**
     * Folder where JBoss is installed.
     */
    private String mInstallRoot;
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());

    /**
     * Called by the JBoss App server at the deployment time of the sar file.  This will
     * create the JBI Framework.
     */
    public void create() {
        mLog.fine("Creating JBI Framework");
        mBlacklistJars.add(JBI_JAR_NAME);
        mBlacklistJars.add(JBI_EXT_JAR_NAME);

        try {
            String serverName = System.getProperty(JBOSS_SERVER_NAME);

            String jbossurl = System.getProperty(JBOSS_HOME_URL);
            URI jbossuri = new URI(jbossurl);
            mInstallRoot = System.getenv("JBOSS_HOME")+"/jboss5-jbi-install";
            mLog.info("Using Jbos Home as default JBI localtion "+mInstallRoot);

            String mInstanceRoot = jbossuri.getPath();
            mEnvironment = new Properties();
            mEnvironment.put("instance.name", serverName);
            mEnvironment.put("instance.root", mInstanceRoot);
            mEnvironment.put("install.root", mInstallRoot);
            createJBIFramework();
            loadJBIFramework();
        } catch (Exception e) {
            mLog.warning("Error creating JBI Framework: " + e.getMessage());
        }
    }

    public void start() {
    }

    public void stop() {
    }

    public void destroy() {
        destroyJBIFramework();
    }

    /**
     * Creates the JBI framework using the appropriate classloading structure.
     * @throws Exception - Exception Creating Framework Class Loader
     */
    private void createJBIFramework() throws Exception {
        Class fwClass;
        Constructor fwCtor;

        try {
            createFrameworkClassLoader();

            fwClass = mFrameworkClassLoader.loadClass(JBI_FRAMEWORK_CLASS_NAME);
            fwCtor = fwClass.getDeclaredConstructor(Properties.class);
            mJbiFramework = fwCtor.newInstance(mEnvironment);
        } catch (Exception ex) {
            throw new Exception("Failed to create JBI framework: " +
                ex.getMessage());
        }
    }

    /**
     *  Creates a separate runtime classloader to avoid namespace pollution
     *  between the component classloading hierarchy and the JBI implementation.
     *  At present, this method is greedy and includes any file in the lib
     *  directory in the runtime classpath.
     */
    private void createFrameworkClassLoader() {
        ArrayList<URL> cpList = new ArrayList<URL>();
        URL[] cpURLs = new URL[0];
        File libDir = new File(mInstallRoot);
        File libDir2 = new File(mInstallRoot, "lib");

        // Everything in the lib directory goes into the classpath
        for (File lib : libDir.listFiles()) {
            try {
                if (mBlacklistJars.contains(lib.getName())) {
                    // skip blacklisted jars
                    continue;
                }

                cpList.add(lib.toURI().toURL());
            } catch (java.net.MalformedURLException urlEx) {
                mLog.warning("Bad library URL: " + urlEx.getMessage());
            }
        }

        for (File lib2 : libDir2.listFiles()) {
            try {
                cpList.add(lib2.toURI().toURL());
                mLog.info("jar "+lib2.toURI().toURL()+" loaded!");
            } catch (MalformedURLException e) {
                mLog.warning("Bad library URL: " + e.getMessage());
            }
        }

        cpURLs = cpList.toArray(cpURLs);
        mFrameworkClassLoader = new URLClassLoader(cpURLs,
                getClass().getClassLoader());
        mLog.info("Framework jars loaded!");
    }

    /**
    *  Loads the JBI framework.
    */
    void loadJBIFramework() {
        try {
            invoke(mJbiFramework, "load");
        } catch (Throwable t) {
            mLog.severe(t.toString());
        }
    }

    /**
     * Destroys the JBI Framework
     */
    private void destroyJBIFramework() {
        try {
            invoke(mJbiFramework, "unload");
        } catch (Throwable t) {
            mLog.severe(t.toString());
        }
    }

    /**
     * This method is used to invoke a given method in a given object using
     * reflection. This bootstrap class uses this method to invoke the load
     * method on the framework.
     * @param obj the object in which the method is to be invoked
     * @param method the method name
     * @param params the list of parameteres needed by the method to be invoked
     * @return - The ojbect that was invoked.
     * @throws Throwable - InvocationTargetException.
     */
    private Object invoke(Object obj, String method, Object... params)
        throws Throwable {
        Object result = null;

        try {
            for (Method m : obj.getClass().getDeclaredMethods()) {
                if (m.getName().equals(method)) {
                    result = m.invoke(obj, params);

                    break;
                }
            }

            return result;
        } catch (java.lang.reflect.InvocationTargetException itEx) {
            throw itEx.getTargetException();
        }
    }
}
