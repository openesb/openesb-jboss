/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBINamingContextImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework.jboss5;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;


/**
 * Naming context implementation for Jboss.
 *
 * @author Sun Microsystems, Inc.
 */
public class JBINamingContextImpl
    implements Context, Serializable
{
    /**
     * Hashtable to store bindings.
     */
    private Hashtable mBindings = null;

    /**
     * Environment.
     */
    private Hashtable mEnvironment = null;

    /**
     * Creates a new instance of JBINamingContextImpl.
     */
    public JBINamingContextImpl()
    {
        mBindings = new Hashtable();
    }

    /**
     * Creates a new JBINamingContextImpl object.
     *
     * @param env environment.
     */
    public JBINamingContextImpl(Hashtable env)
    {
        this.mEnvironment = env;
    }

    /**
     * Returns the environment.
     *
     * @return environemtn table.
     *
     * @throws javax.naming.NamingException if environment is not set.
     */
    public java.util.Hashtable getEnvironment()
        throws javax.naming.NamingException
    {
        return this.mEnvironment;
    }

    /**
     * Returns the binding within a namespace.
     *
     * @return binding.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public String getNameInNamespace()
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Gets the name parser.
     *
     * @param name name.
     *
     * @return name parser.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public javax.naming.NameParser getNameParser(javax.naming.Name name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Gets the name parser.
     *
     * @param name string representation of name.
     *
     * @return Name parser.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public javax.naming.NameParser getNameParser(String name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Adds the binding to the environment values.
     *
     * @param propName Property name.
     * @param propVal value.
     *
     * @return object that has been added.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public Object addToEnvironment(
        String propName,
        Object propVal)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     * @param obj Not supported.Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public void bind(
        javax.naming.Name name,
        Object obj)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Binds the name to the object.
     *
     * @param name key name. 
     * @param obj object.
     *
     * @throws javax.naming.NamingException if name is null or not valid.
     */
    public void bind(
        String name,
        Object obj)
        throws javax.naming.NamingException
    {
        try
        {
            this.mBindings.put(name, obj);
        }
        catch (Exception e)
        {
            throw new javax.naming.NamingException("Bind failed "
                + e.getMessage());
        }
    }

    /**
     * Closes the hash table. 
     *
     * @throws javax.naming.NamingException if bindings is not intitialised.
     */
    public void close()
        throws javax.naming.NamingException
    {
        this.mBindings.clear();
    }

    /**
     * 
     * Not supported method.
     * @param name Not supported
     * @param prefix Not supported
     *
     * @return Not supported
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public javax.naming.Name composeName(
        javax.naming.Name name,
        javax.naming.Name prefix)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     * @param prefix Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public String composeName(
        String name,
        String prefix)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public Context createSubcontext(javax.naming.Name name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public Context createSubcontext(String name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public void destroySubcontext(javax.naming.Name name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public void destroySubcontext(String name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException if name not found.
     */
    public javax.naming.NamingEnumeration list(String name)
        throws javax.naming.NamingException
    {
        Enumeration enumr = null;

        try
        {
            enumr = this.mBindings.elements();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new NamingException(e.getMessage());
        }

        return new NamingEnumerationImpl(enumr);
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public javax.naming.NamingEnumeration list(javax.naming.Name name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public javax.naming.NamingEnumeration listBindings(String name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public javax.naming.NamingEnumeration listBindings(javax.naming.Name name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Retrieves the value corresponding to the key name.
     *
     * @param name key name.
     *
     * @return object value.
     *
     * @throws javax.naming.NamingException if name not found.
     */
    public Object lookup(String name)
        throws javax.naming.NamingException
    {
        Object obj = null;

        try
        {
            obj = this.mBindings.get(name);
        }
        catch (Exception e)
        {
            throw new javax.naming.NamingException("Lookup failed "
                + e.getMessage());
        }

        return obj;
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public Object lookup(javax.naming.Name name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public Object lookupLink(javax.naming.Name name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public Object lookupLink(String name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     * @param obj Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public void rebind(
        String name,
        Object obj)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param name Not supported.
     * @param obj Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public void rebind(
        javax.naming.Name name,
        Object obj)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported. 
     *
     * @param propName Not supported.
     *
     * @return Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public Object removeFromEnvironment(String propName)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param oldName Not supported. 
     * @param newName Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public void rename(
        javax.naming.Name oldName,
        javax.naming.Name newName)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Not supported.
     *
     * @param oldName Not supported. 
     * @param newName Not supported.
     *
     * @throws javax.naming.NamingException Not supported. 
     * @throws OperationNotSupportedException Not supported.
     */
    public void rename(
        String oldName,
        String newName)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     * Unbinds the key and value.
     *
     * @param name name to unbind.
     *
     * @throws javax.naming.NamingException if name not found.
     */
    public void unbind(String name)
        throws javax.naming.NamingException
    {
        Object obj = null;

        try
        {
            this.mBindings.remove(name);
        }
        catch (Exception e)
        {
            throw new NamingException("Cannot unbind " + e.getMessage());
        }
    }

    /**
     * Not supported. 
     *
     * @param name Not supported.
     *
     * @throws javax.naming.NamingException Not supported.
     * @throws OperationNotSupportedException Not supported.
     */
    public void unbind(javax.naming.Name name)
        throws javax.naming.NamingException
    {
        throw new OperationNotSupportedException();
    }

    /**
     *   Enumeration implementation.
     *
     * @author Sun Microsystems, Inc.
     * 
     */
    private class NamingEnumerationImpl
        implements NamingEnumeration
    {
        /**
         *    
         */
        private Enumeration mEnumeration = null;

        /**
         * Creates a new NamingEnumerationImpl object.
         *
         * @param mEnumeration   enumeration.
         */
        NamingEnumerationImpl(Enumeration mEnumeration)
        {
            this.mEnumeration = mEnumeration;
        }

        /**
         * Closes the mEnumeration.
         *
         * @throws NamingException naming exception.
         */
        public void close()
            throws NamingException
        {
            this.mEnumeration = null;
        }

        /**
         * If there are more items.
         *
         * @return  true if there is.
         *
         * @throws NamingException  naming exception.
         */
        public boolean hasMore()
            throws NamingException
        {
            return this.hasMoreElements();
        }

        /**
         * Retunrs true if the mEnumeration has more elements.
         *
         * @return  true if there are more elements.
         */
        public boolean hasMoreElements()
        {
           return this.mEnumeration.hasMoreElements();
        }

        /**
         * Retrieves the next item in the mEnumeration.
         *
         * @return  next item.
         *
         * @throws NamingException    naming exception.
         */
        public Object next()
            throws NamingException
        {
            return this.nextElement();
        }

        /**
         * Retuens the next element in the mEnumeration.
         *
         * @return  object.
         */
        public Object nextElement()
        {
            return this.mEnumeration.nextElement();
        }
    }
}
