package com.sun.jbi.framework.jboss5.deploy;

import org.jboss.deployers.spi.DeploymentException;
import org.jboss.deployers.spi.deployer.DeploymentStages;
import org.jboss.deployers.vfs.spi.deployer.AbstractOptionalVFSRealDeployer;
import org.jboss.deployers.vfs.spi.structure.VFSDeploymentUnit;
import org.jboss.metadata.web.jboss.JBossWebMetaData;
import org.jboss.util.StringPropertyReplacer;
import org.jboss.virtual.VFS;
import org.jboss.virtual.VirtualFile;
import org.jboss.virtual.VirtualFileFilter;
import org.jboss.virtual.plugins.vfs.helpers.SuffixMatchFilter;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class OpenESBDeployer extends AbstractOptionalVFSRealDeployer<JBossWebMetaData> {
    private static final VirtualFileFilter JAR_FILTER = new SuffixMatchFilter(".jar");

    /** Extensions directory containing the jars where that will be added to 
     * the OpenESB SARs classpath. */
    private String extDir = null;
    /** Name of the OpenESB SAR, used to filter the deployment being modifed. */
    private String sarName = null;   
   
    public OpenESBDeployer() {
        super(JBossWebMetaData.class);
        setStage(DeploymentStages.POST_PARSE); // pre-classloading
        this.log.info("Initialized OpenESBDeployer");
    }

    public void setExtDir(String extDir) {
        this.extDir = System.getenv("JBOSS_HOME")+extDir;
    }

    public void setSarName(String sarName) {
        this.sarName = sarName.toLowerCase();
    }

    @Override
    public void deploy(VFSDeploymentUnit unit, JBossWebMetaData metaData) 
    throws DeploymentException {
        if(!this.sarName.equalsIgnoreCase(unit.getSimpleName())) { 
            return;
        }

        try {
            final String extDirPath = StringPropertyReplacer.replaceProperties(extDir);
            final URL extDirURL = new File(extDirPath).toURI().toURL();

            loadExtDir(extDirURL, unit);

            this.log.info("Added jars from " + extDirPath + " to the classpath of " + unit.getName());
        } catch (MalformedURLException e) {
            throw DeploymentException.rethrowAsDeploymentException("Malformed URL", e);
        } catch (IOException e) {
            throw DeploymentException.rethrowAsDeploymentException("Error during loading of ext dir", e);
        }

    }

    /** Loads the jars contained in extDirURL to the classpath of the provided 
     * unit. */
    private void loadExtDir(URL extDirURL, VFSDeploymentUnit                                 unit)
    throws DeploymentException, IOException {
        final VirtualFile vExtDir = VFS.getRoot(extDirURL);

        if(vExtDir.isLeaf()) {
            return;
            //throw new DeploymentException(vExtDir + " is not a valid directory");
        }

        for(VirtualFile jarFile : vExtDir.getChildrenRecursively(JAR_FILTER)) {
            this.log.debug("Loading jar: " + jarFile);
            unit.addClassPath(jarFile);
        }
    }
}

